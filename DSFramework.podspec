#
# Be sure to run `pod lib lint DSFramework.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'DSFramework'
    s.version          = '0.1.0'
    s.summary          = 'Group of features packed together to speed up app development.'
    
    # This description is used to generate tags and improve search results.
    #   * Think: What does it do? Why did you write it? What is the focus?
    #   * Try to keep it short, snappy and to the point.
    #   * Write the description between the DESC delimiters below.
    #   * Finally, don't worry about the indent, CocoaPods strips it!
    
    s.description      = <<-DESC
    'Group of features packed together to speed up app development.'
    DESC
    
    s.homepage         = 'http://davidebalistreri.it'
    # s.screenshots    = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Davide Balistreri' => 'davidebalistreri@hotmail.it' }
    s.source           = { :git => 'https://bitbucket.org/davidebalistreri/dsframework.git', :tag => s.version.to_s }
    
    s.ios.deployment_target = '9.0'
    
    s.source_files = 'DSFramework/Classes/**/*'
    
    # s.resource_bundles = {
    #   'DSFramework' => ['DSFramework/Assets/*.png']
    # }
    
    # s.public_header_files = 'Pod/Classes/**/*.h'
    # s.frameworks = 'UIKit', 'MapKit'
    
    s.dependency 'Alamofire', '4.8.1'
end
