//
//  DSAttributedString.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit

open class DSAttributedString {
    
    open private(set) var string: NSMutableAttributedString

    private var paragraphStyle: NSMutableParagraphStyle
    
    public init(_ string: String) {
        self.string = NSMutableAttributedString(string: string)
        self.paragraphStyle = NSMutableParagraphStyle()
    }
    
    
    // MARK: - Set
    
    open func setFont(_ font: UIFont) {
        let range = NSMakeRange(0, string.length)
        replaceFont(font, of: range)
    }
    
    open func setTextColor(_ color: UIColor) {
        let range = NSMakeRange(0, string.length)
        replaceTextColor(color, of: range)
    }
    
    open func set(value: Any, forAttribute attribute: NSAttributedString.Key) {
        let range = NSMakeRange(0, string.length)
        replace(value: value, forAttribute: attribute, of: range)
    }
    
    
    // MARK: - Replace with range
    
    open func replaceFont(_ font: UIFont, of range: NSRange) {
        replace(value: font, forAttribute: .font, of: range)
    }
    
    open func replaceTextColor(_ color: UIColor, of range: NSRange) {
        replace(value: color, forAttribute: .foregroundColor, of: range)
    }
    
    open func replace(value: Any, forAttribute attribute: NSAttributedString.Key, of range: NSRange) {
        string.addAttributes([attribute: value], range: range)
    }
    
    
    // MARK: - Replace with string
    
    open func replaceFont(_ font: UIFont, of substring: String) {
        let string = self.string.string as NSString
        let range = string.range(of: substring)
        replaceFont(font, of: range)
    }
    
    open func replaceTextColor(_ color: UIColor, of substring: String) {
        let string = self.string.string as NSString
        let range = string.range(of: substring)
        replaceTextColor(color, of: range)
    }
    
    open func replace(value: Any, forAttribute attribute: NSAttributedString.Key, of substring: String) {
        let string = self.string.string as NSString
        let range = string.range(of: substring)
        replace(value: value, forAttribute: attribute, of: range)
    }
    
    
    // MARK: - Complex attributes
    
    open func setLineSpacing(_ lineSpacing: CGFloat) {
        paragraphStyle.lineSpacing = lineSpacing
        set(value: paragraphStyle, forAttribute: .paragraphStyle)
    }
    
    open func setLineBreakMode(_ lineBreakMode: NSLineBreakMode) {
        paragraphStyle.lineBreakMode = lineBreakMode
        set(value: paragraphStyle, forAttribute: .paragraphStyle)
    }
    
    open func setAlignment(_ alignment: NSTextAlignment) {
        paragraphStyle.alignment = alignment
        set(value: paragraphStyle, forAttribute: .paragraphStyle)
    }
    
}
