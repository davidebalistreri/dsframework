//
//  DSStringsExtension.swift
//  DSFramework
//
//  Created by Davide Balistreri on 26/02/18.
//

import UIKit

public extension String {
    
    static func safe(_ string: String?) -> String {
        if let safeString = string {
            return safeString
        } else {
            return ""
        }
    }
    
    static func cleaning(_ string: String?) -> String {
        let safeString = safe(string)
        return safeString.trimmed
    }
    
    var trimmed: String {
        get {
            return trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
    
}
