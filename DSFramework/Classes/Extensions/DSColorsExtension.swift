//
//  DSColorsExtension.swift
//  DSFramework
//
//  Created by Davide Balistreri on 26/02/18.
//

import UIKit

public extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    convenience init(red r: Int, green g: Int, blue b: Int, alpha: CGFloat) {
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    convenience init(hex: String) {
        var safeHex = String.cleaning(hex)
        safeHex = safeHex.replacingOccurrences(of: "#", with: "")
        
        let scanner = Scanner(string: safeHex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        let red   = CGFloat(r) / 0xff
        let green = CGFloat(g) / 0xff
        let blue  = CGFloat(b) / 0xff
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
}
