//
//  DSNetworkingDefines.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//


public typealias DSNetworkingURL = String

public typealias DSNetworkingHeaders = [String: String]

public typealias DSNetworkingParameters = [String: Any]

public typealias DSNetworkingCompletion = ((DSNetworkingResponse) -> Void)


public enum DSNetworkingHTTPMethod {
    case get
    case post
    case put
    case delete
}


public enum DSNetworkingParameterEncoding {
    /// Default. URLEncoding for GET requests, JSONEncoding for all others.
    case automatic
    /// application/x-www-form-urlencoded
    case url
    /// application/json
    case json
}


public enum DSNetworkingCacheMode {
    /// Default. Fastest app responsiveness.
    case returnCacheAndReload
    /// Ignores local cache and retrieves data from the server.
    case noCache
}
