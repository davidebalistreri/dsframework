//
//  DSNetworkingScheduler.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit


/**
 * - Request scheduling: multiple download requests may slow down and overload both the server and the application. A smart scheduling system handles all requests to run in the most efficent way.
 *
 * - Duplicate requests: sometimes applications may trigger multiple identical download requests, which can lead to increased network and memory usage. DSNetworking prevents this behavior by merging all duplicate requests into one, yet calling all provided completion handlers.
 */


internal class DSNetworkingScheduler {
    
}
