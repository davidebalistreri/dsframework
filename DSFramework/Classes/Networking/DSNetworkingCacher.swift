//
//  DSNetworkingCacher.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit


/**
 * - Response caching: DSNetworking includes a smart caching system that helps applications to perform as fast as possible. Whenever a request is launched for the second time, it will instantly return the last cached response, then it'll proceed by reaching the server for the updated response data.
 */


internal class DSNetworkingCacher {
    
    static func store(request: DSNetworkingRequest, response: DSNetworkingResponse) {
        guard let serializedRequest = DSNetworkingCacher.serializedRequest(request) else {
            return
        }
        
        guard let serializedResponse = DSNetworkingCacher.serializedResponse(response) else {
            return
        }
        
        let defaults = UserDefaults.standard
        defaults.set(serializedResponse, forKey: serializedRequest)
        defaults.synchronize()
    }
    
    static func cachedResponse(withRequest request: DSNetworkingRequest) -> DSNetworkingResponse? {
        guard let serializedRequest = DSNetworkingCacher.serializedRequest(request) else {
            return nil
        }
        
        let defaults = UserDefaults.standard
        let serializedResponse = defaults.string(forKey: serializedRequest)
        return DSNetworkingCacher.unserializedResponse(serializedResponse, withRequest: request)
    }
    
    
    // MARK: - Helpers
    
    private static func serializedRequest(_ request: DSNetworkingRequest) -> String? {
        guard request.method == .get else {
            // Invalid request
            return nil
        }
        
        let string = NSMutableString()
        string.append("DSNetworkingCacher|")
        
        string.append("\(request.stringParameterEncoding)|")
        string.append("\(request.stringMethod)|")
        string.append("\(request.stringUrl)|")
        
        if let headers = request.stringHeaders {
            string.append("\(headers)|")
        }
        
        if let parameters = request.stringParameters {
            string.append("\(parameters)|")
        }
        
        return String(string)
    }
    
    private static func serializedResponse(_ response: DSNetworkingResponse) -> String? {
        guard response.success, let responseObject = response.responseObject else {
            // Invalid response
            return nil
        }
        
        do {
            let responseData = try JSONSerialization.data(withJSONObject: responseObject, options: [])
            return String(data: responseData, encoding: .utf8)
        } catch {
            // Something went wrong
            return nil
        }
    }
    
    private static func unserializedResponse(_ serializedResponse: String?, withRequest request: DSNetworkingRequest) -> DSNetworkingResponse? {
        guard let serializedResponse = serializedResponse, let responseData = serializedResponse.data(using: .utf8) else {
            // Invalid response
            return nil
        }
        
        let response = DSNetworkingResponse(request: request)
        response.isCached = true
        response.success = true
        response.statusCode = 200
        
        // Body
        do {
            let responseObject = try JSONSerialization.jsonObject(with: responseData, options: .mutableContainers)
            response.responseObject = responseObject
        } catch {
            // Something went wrong
            return nil
        }
        
        return response
    }
    
}


private extension DSNetworkingRequest {
    
    var stringUrl: String {
        get {
            if let url = url {
                return url
            } else {
                return ""
            }
        }
    }
    
    var stringParameterEncoding: String {
        get {
            switch parameterEncoding {
            case .automatic:
                return "automatic"
            case .url:
                return "url"
            case .json:
                return "json"
            }
        }
    }
    
    var stringMethod: String {
        get {
            switch method {
            case .post:
                return "post"
            case .put:
                return "put"
            case .delete:
                return "delete"
            default:
                return "get"
            }
        }
    }
    
    var stringHeaders: String? {
        get {
            guard let headers = headers, headers.count > 0 else {
                // No headers
                return nil
            }
            
            do {
                let data = try JSONSerialization.data(withJSONObject: headers, options: [])
                return String(data: data, encoding: .utf8)
            } catch {
                // Something went wrong
                return nil
            }
        }
    }
    
    var stringParameters: String? {
        get {
            guard let parameters = parameters, parameters.count > 0 else {
                // No parameters
                return nil
            }
            
            do {
                let data = try JSONSerialization.data(withJSONObject: parameters, options: [])
                return String(data: data, encoding: .utf8)
            } catch {
                // Something went wrong
                return nil
            }
        }
    }
    
}
