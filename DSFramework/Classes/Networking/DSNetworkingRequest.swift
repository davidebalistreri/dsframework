//
//  DSNetworkingRequest.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit


open class DSNetworkingRequest {
    
    // MARK: - Basic setup
    
    open var url: DSNetworkingURL?
    
    open var method: DSNetworkingHTTPMethod = .get
    
    
    open var headers: DSNetworkingHeaders?
    
    open var parameters: DSNetworkingParameters?
    
    open var completion: DSNetworkingCompletion?
    
    
    // MARK: - Advanced setup
    
    open var timeoutInterval: TimeInterval
    
    open var parameterEncoding: DSNetworkingParameterEncoding = .automatic
    
    open var numberOfRetries: Int
    
    open var cacheMode: DSNetworkingCacheMode
    
    
    // MARK: - Logging
    
    open var timestamp: TimeInterval?
    
    open var isLogEnabled = false
    
    
    // MARK: - Actions
    
    open func execute() {
        self.timestamp = Date().timeIntervalSinceReferenceDate
        DSNetworkingCore.execute(request: self)
    }
    
    
    public init() {
        self.headers = DSNetworking.defaultHeaders
        self.timeoutInterval = DSNetworking.defaultTimeoutInterval
        self.parameterEncoding = DSNetworking.defaultParameterEncoding
        self.numberOfRetries = DSNetworking.defaultNumberOfRetries
        self.cacheMode = DSNetworking.defaultCacheMode
        self.isLogEnabled = DSNetworking.isLogEnabled
    }
    
    public convenience init(url: DSNetworkingURL?, method: DSNetworkingHTTPMethod) {
        self.init()
        self.url = url
        self.method = method
    }
    
}
