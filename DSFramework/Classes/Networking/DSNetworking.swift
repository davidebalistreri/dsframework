//
//  DSNetworking.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import Foundation
import Alamofire


/**
 * Main features:
 *
 * - Networking made simple: all common requests are ready to use and developers can easily customize all request parameters as needed.
 *
 * - Response caching: DSNetworking includes a smart caching system that helps applications to perform as fast as possible. Whenever a request is launched for the second time, it will instantly return the last cached response, then it'll proceed by reaching the server for the updated response data.
 *
 * - Request queue scheduling: multiple download requests may slow down and overload both the server and the application. A smart scheduling system handles all requests to run in the most efficent way.
 *
 * - Duplicate requests: sometimes applications may trigger multiple identical download requests, which can lead to increased network and memory usage. DSNetworking prevents this behavior by merging all duplicate requests into one, yet calling all provided completion handlers.
 *
 * - Other: DSNetworking features also a smart NetworkActivityIndicator.
 */


public class DSNetworking {
    
    // MARK: - Setup
    
    public static var defaultHeaders: DSNetworkingHeaders?
    
    public static var defaultParameterEncoding: DSNetworkingParameterEncoding = .automatic
    
    public static var defaultTimeoutInterval: TimeInterval = 60.0
    
    public static var defaultNumberOfRetries: Int = 0
    
    public static var defaultCacheMode: DSNetworkingCacheMode = .returnCacheAndReload
    
    public static var isLogEnabled: Bool = false
    
    /// Disable this parameter to accept invalid or self-signed SSL certificates
    public static var isResponseSecurityValidationEnabled: Bool = true
    
    
    // MARK: - Fast requests
    
    @discardableResult public static func JSONRequest(method: DSNetworkingHTTPMethod, url: DSNetworkingURL, parameters: DSNetworkingParameters?, completion: DSNetworkingCompletion?) -> DSNetworkingRequest {
        
        let request = DSNetworkingRequest(url: url, method: method)
        request.parameters = parameters
        request.completion = completion
        
        request.execute()
        
        return request
    }
    
}
