//
//  DSNetworkingLogger.swift
//  DSFramework
//
//  Created by Davide Balistreri on 28/02/18.
//

import UIKit


internal class DSNetworkingLogger {
    
    static func log(_ response: DSNetworkingResponse) {
        let string = NSMutableString()
        
        let request = response.request
        
        // METHOD
        switch request.method {
        case .post:
            string.append("POST ")
        case .put:
            string.append("PUT ")
        case .delete:
            string.append("DELETE ")
        default:
            string.append("GET ")
        }
        
        // URL
        string.append("URL: ")
        string.append(request.url ?? "")
        string.append("\n")
        
        // STATUS CODE
        let statusCode = response.statusCode
        let statusCodeDescription = HTTPURLResponse.localizedString(forStatusCode: statusCode).capitalized
        string.append("STATUS CODE: \(statusCode) \(statusCodeDescription)")
        string.append("\n")
        
        // ELAPSED TIME
        if let requestTimestamp = request.timestamp {
            let elapsed = (response.timestamp - requestTimestamp) * 1000.0
            string.append("TIME: ")
            string.appendFormat("%.f ms", elapsed)
            string.append("\n")
        }
        
        if let headers = request.headers {
            string.append("HEADERS:")
            string.append("\n")
            string.append(DSNetworkingLogger.jsonString(withObject: headers))
            string.append("\n")
        } else {
            string.append("NO HEADERS")
            string.append("\n")
        }
        
        if let parameters = request.parameters {
            string.append("PARAMETERS:")
            string.append("\n")
            string.append(DSNetworkingLogger.jsonString(withObject: parameters))
            string.append("\n")
        } else {
            string.append("NO PARAMETERS")
            string.append("\n")
        }
        
        if let responseObject = response.responseObject {
            string.append("RESPONSE:")
            string.append("\n")
            string.append(DSNetworkingLogger.jsonString(withObject: responseObject))
            string.append("\n")
        } else {
            string.append("NO RESPONSE")
            string.append("\n")
        }
        
        NSLog("") // Timestamp
        print(string) // Full log print
    }
    
    private static func jsonString(withObject object: Any) -> String {
        
        guard JSONSerialization.isValidJSONObject(object) else {
            // Invalid object
            return ""
        }
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
            return String(data: jsonData, encoding: .utf8) ?? ""
        } catch {
            return ""
        }
    }
    
}
