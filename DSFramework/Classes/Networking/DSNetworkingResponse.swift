//
//  DSNetworkingResponse.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit


open class DSNetworkingResponse {
    
    // MARK: - Basic info
    
    open var request: DSNetworkingRequest
    
    
    open var success: Bool
    
    open var statusCode: Int
    
    open var error: Error?
    
    open var responseObject: Any?
    
    
    // MARK: - Advanced info
    
    open var isCached = false
    
    open var retryCount: Int = 0
    
    
    // MARK: - Logging
    
    open var timestamp: TimeInterval = Date().timeIntervalSinceReferenceDate
    
    
    public init(request: DSNetworkingRequest) {
        self.request = request
        self.success = false
        self.statusCode = 0
    }
    
}


public extension DSNetworkingResponse {
    
    convenience init(request: DSNetworkingRequest, success: Bool, statusCode: Int) {
        self.init(request: request)
        
        self.success = success
        self.statusCode = statusCode
    }
    
}
