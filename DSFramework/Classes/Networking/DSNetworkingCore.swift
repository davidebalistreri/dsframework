//
//  DSNetworkingCore.swift
//  DSFramework
//
//  Created by Davide Balistreri on 17/02/18.
//

import UIKit
import Alamofire


internal class DSNetworkingCore {
    
    private static let alamofireManager: SessionManager = {
        // Manager initialization
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        
        let manager = SessionManager(configuration: configuration, serverTrustPolicyManager: DSAlamofirePolicyManager())
        manager.startRequestsImmediately = false
        
        return manager
    }()
    
    
    static func execute(request: DSNetworkingRequest) {
        
        // Cache time!
        if request.cacheMode == .returnCacheAndReload {
            if let cachedResponse = DSNetworkingCacher.cachedResponse(withRequest: request) {
                request.completion?(cachedResponse)
            }
        }
        
        // Server time!
        var alamofireRequest = alamofireManager.request(request.properUrl, method: request.properMethod, parameters: request.parameters, encoding: request.properEncoding, headers: request.headers)
        
        // Applying custom timeoutInterval
        if var alamofireInternalRequest = alamofireRequest.request {
            alamofireInternalRequest.timeoutInterval = request.timeoutInterval
            alamofireRequest = alamofireManager.request(alamofireInternalRequest)
        }
        
        // Defining response handler
        alamofireRequest.responseJSON {
            (alamofireResponse) in
            
            let response = DSNetworkingResponse(request: request, alamofireResponse: alamofireResponse)
            
            // Cache time!
            if request.cacheMode == .returnCacheAndReload {
                DSNetworkingCacher.store(request: request, response: response)
            }
            
            // Log
            if request.isLogEnabled {
                DSNetworkingLogger.log(response)
            }
            
            // Completion handler
            request.completion?(response)
        }
        
        // Executing the request
        alamofireRequest.resume()
    }
    
}


private extension DSNetworkingRequest {
    
    var properUrl: URLConvertible {
        get {
            if let url = url {
                return url
            } else {
                return ""
            }
        }
    }
    
    var properMethod: HTTPMethod {
        get {
            switch method {
            case .post:
                return .post
            case .put:
                return .put
            case .delete:
                return .delete
            default:
                return .get
            }
        }
    }
    
    var properEncoding: ParameterEncoding {
        get {
            if parameterEncoding == .url {
                return URLEncoding.default
            }
            else if parameterEncoding == .json {
                return JSONEncoding.default
            }
            else {
                // Automatic
                if method == .get {
                    return URLEncoding.default
                } else {
                    return JSONEncoding.default
                }
            }
        }
    }
    
}


extension DSNetworkingResponse {
    
    convenience init(request: DSNetworkingRequest, alamofireResponse: DataResponse<Any>) {
        self.init(request: request)
        
        if let statusCode = alamofireResponse.response?.statusCode {
            self.success = (statusCode >= 200 && statusCode < 300) ? true : false
            self.statusCode = statusCode
        }
        
        self.responseObject = alamofireResponse.result.value
        self.error = alamofireResponse.error
    }
    
}


internal class DSAlamofirePolicyManager : ServerTrustPolicyManager {
    
    convenience init() {
        self.init(policies: [:])
    }
    
    override func serverTrustPolicy(forHost host: String) -> ServerTrustPolicy? {
        
        if !DSNetworking.isResponseSecurityValidationEnabled {
            return ServerTrustPolicy.disableEvaluation
        }
        
        return super.serverTrustPolicy(forHost: host)
    }
    
}
