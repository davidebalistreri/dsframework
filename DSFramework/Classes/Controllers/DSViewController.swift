//
//  DSViewController.swift
//  DSFramework
//
//  Created by Davide Balistreri on 02/03/2018
//  Copyright © 2018 Davide Balistreri. All rights reserved.
//

import UIKit

open class DSViewController : UIViewController {
    
    open var isSetupCompleted = false
    
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isSetupCompleted {
            // L'inizializzazione del ViewController è quasi completa
            
            // Eseguo i metodi implementati dallo sviluppatore
            setupControllerViews()
            localizeControllerViews()
        }
        
        updateControllerViews(animated)
        
        // Inizializzazione del ViewController completata
        isSetupCompleted = true
    }
    
    
    open func setupControllerViews() {
        // Developers may override this method to provide custom behavior
    }
    
    open func localizeControllerViews() {
        // Developers may override this method to provide custom behavior
    }
    
    open func updateControllerViews(_ animated: Bool) {
        // Developers may override this method to provide custom behavior
    }
    
}
