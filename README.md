# DSFramework

## Installation

DSFramework is available through [CocoaPods](http://cocoapods.org).
To install it, simply add the following line to your Podfile:

```ruby
pod 'DSFramework', :git => 'https://bitbucket.org/davidebalistreri/dsframework.git'
```

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Author

Davide Balistreri, davidebalistreri@hotmail.it

## License

DSFramework is available under the MIT license. See the LICENSE file for more info.
